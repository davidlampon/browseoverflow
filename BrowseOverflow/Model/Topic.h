//
//  Topic.h
//  BrowseOverflow
//
//  Created by David Lampon Diestre on 24/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface Topic : NSObject

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *tag;

- (id)initWithName:(NSString *)newName tag:(NSString *)newTag;

- (NSArray *)recentQuestions;

- (void)addQuestion: (Question *)question;

@end