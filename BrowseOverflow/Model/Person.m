//
//  Person.m
//  BrowseOverflow
//
//  Created by David Lampon Diestre on 24/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import "Person.h"

@implementation Person

@synthesize name;
@synthesize avatarURL;

- (id)initWithName:(NSString *)aName avatarLocation:(NSString *)location
{
    if ((self = [super init]))
    {
        name = [aName copy];
        avatarURL = [[NSURL alloc] initWithString: location];
    }
    
    return self;
}

@end


