//
//  Answer.h
//  BrowseOverflow
//
//  Created by David Lampon Diestre on 25/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface Answer : NSObject

@property (nonatomic) NSString *text;
@property (nonatomic) Person *person;
@property (nonatomic) NSInteger score;
@property (nonatomic, getter=isAccepted) BOOL accepted;

- (NSComparisonResult)compare:(Answer *)otherAnswer;

@end
