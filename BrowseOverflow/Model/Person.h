//
//  Person.h
//  BrowseOverflow
//
//  Created by David Lampon Diestre on 24/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSURL *avatarURL;

- (id)initWithName:(NSString *)aName avatarLocation:(NSString *)location;

@end
