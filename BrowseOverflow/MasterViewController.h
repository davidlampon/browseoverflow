//
//  MasterViewController.h
//  BrowseOverflow
//
//  Created by David Lampon Diestre on 24/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController

@end
