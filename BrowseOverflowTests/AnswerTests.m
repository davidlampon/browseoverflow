//
//  Answers.m
//  BrowseOverflow
//
//  Created by David Lampon Diestre on 25/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Answer.h"

@interface AnswerTests : XCTestCase

@end

@implementation AnswerTests
{
    Answer *answer;
    Answer *otherAnswer;
}

- (void)setUp
{
    answer = [[Answer alloc] init];
    answer.text = @"The answer is 42";
    answer.person = [[Person alloc] initWithName: @"Graham Lee"
                                  avatarLocation: @"http://example.com/avatar.png"];
    answer.score = 42;
    otherAnswer = [[Answer alloc] init];
    otherAnswer.text = @"I have the answer you need";
    otherAnswer.score = 42;
}

- (void)testAcceptedAnswerComesBeforeUnaccepted
{
    otherAnswer.accepted = YES;
    otherAnswer.score = answer.score + 10;
    XCTAssertEqual([answer compare: otherAnswer], NSOrderedDescending,
                   @"Accepted answer should come first");
    XCTAssertEqual([otherAnswer compare: answer], NSOrderedAscending,
                   @"Unaccepted answer should come last");
}

- (void)testAnswersWithEqualScoresCompareEqually
{
    XCTAssertEqual([answer compare: otherAnswer], NSOrderedSame,
                   @"Both answers of equal rank");
    XCTAssertEqual([otherAnswer compare: answer], NSOrderedSame,
                   @"Each answer has the same rank");
}

- (void)testLowerScoringAnswerComesAfterHigher {
    otherAnswer.score = answer.score + 10;
    XCTAssertEqual([answer compare: otherAnswer], NSOrderedDescending,
                   @"Higher score comes first");
    XCTAssertEqual([otherAnswer compare: answer], NSOrderedAscending,
                   @"Lower score comes last");
}

@end
