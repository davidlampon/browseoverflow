//
//  PersonTests.m
//  BrowseOverflow
//
//  Created by David Lampon Diestre on 24/10/13.
//  Copyright (c) 2013 David Lampon Diestre. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Person.h"

@interface PersonTests : XCTestCase

@end

@implementation PersonTests
{
    Person *person;
}

- (void)setUp
{
    person = [[Person alloc] initWithName: @"Graham Lee"
                           avatarLocation: @"http://example.com/avatar.png"];
}

- (void)tearDown
{
    person = nil;
}

- (void)testThatPersonHasTheRightName
{
    XCTAssertEqualObjects(person.name, @"Graham Lee",
                         @"expecting a person to provide its name");
}

- (void)testThatPersonHasAnAvatarURL
{
    NSURL *url = person.avatarURL;
    XCTAssertEqualObjects([url absoluteString],
                         @"http://example.com/avatar.png",
                         @"The Person’s avatar should be represented by a URL");
}

@end
